# 简介

本脚本从手机pull跟踪数据到当前工作目录的data子目录下，再对比*_fingerMarks.txt和
*_fingerMarks_res.txt的结果到*_fingerMarks_res.csv。

# 操作说明

运行：$ ot_report.py
帮助：$ ot_report.py -h


 vim:ft=markdown
